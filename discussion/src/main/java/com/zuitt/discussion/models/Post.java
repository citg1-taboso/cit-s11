package com.zuitt.discussion.models;

import javax.persistence.*;

// makes an object representation of from the database
// basically creates a table
@Entity
//designate the table name related to the model
@Table(name = "posts")
public class Post {
//  Properties
//  Indicates the primary key of the table
    @Id
//  Specify an auto increment value
    @GeneratedValue
    private Long id;
//  Class properties that represents table columns in a RDB
    @Column
    private String title;
    @Column
    private String content;

    public Post() {
        //default constructor
    }

    public Post(String title, String content) {
        this.title = title;
        this.content = content;
    }
//    Getters and Setters
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }


}
